package com.example.cocktailapplication.controllers;

import com.example.cocktailapplication.domain.Cocktail;
import com.example.cocktailapplication.data.SearchService;
import com.example.cocktailapplication.repos.CocktailRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
//import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

   private SearchService searchService;
   private List<Cocktail> cocktails;
   private List<Cocktail> randomCocktailsList;
   private List<Cocktail> list1;

   @Autowired
   private CocktailRepo cocktailRepo;


  @GetMapping("/")
    public String greeting( Model model) throws IOException {
       // ClassPathXmlApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");context.getBean("jsonParser", JSONParser.class);
        searchService= new SearchService();
        cocktails=searchService.getRandomCocktails();
        list1= new ArrayList<>();
        for (int i=0; i<3; i++){
            list1.add(cocktails.get(i));
        }
        model.addAttribute("searchService", new SearchService());
        model.addAttribute("cocktails", list1);

        return "home";
    }

    @GetMapping("/cocktail")
    public String showCocktail1(@RequestParam(name= "id") int id, Model model) {
        model.addAttribute("cocktail", cocktails.get(id));

        return "cocktail";
  }

   @RequestMapping(value = { "/" }, method = RequestMethod.POST)
    public String searchCocktailByName(Model model,
                             @ModelAttribute("searchService") SearchService currentSearchService) throws IOException {

        model.addAttribute("cocktails", currentSearchService.getData());
        model.addAttribute("searchService", new SearchService());
        return "home";

        }

        @GetMapping
     public  String getDataFromDB(Map<String,Object> model){
      Iterable<Cocktail> cocktails= cocktailRepo.findAll();
      model.put("cocktails",cocktails);
      return "database";
        }


    @PostMapping
    public String add(@RequestParam(name= "id") int id, Map<String,Object> model) {
        Cocktail cocktail= cocktails.get(id);
        cocktailRepo.save(cocktail);
        Iterable<Cocktail> cocktails= cocktailRepo.findAll();
        model.put("cocktails", cocktails);

        return "database";
    }
}

