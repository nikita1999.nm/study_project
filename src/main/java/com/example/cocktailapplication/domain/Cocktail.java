package com.example.cocktailapplication.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Cocktail {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private String name;
    private String imageUrl;
    private int id;

    public Cocktail(){}

    private String instruction;
    public Cocktail (String name, String imageUrl, int id, String instruction){
        this.name=name;
        this.imageUrl=imageUrl;
        this.id= id;
        this.instruction=instruction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getUrl() {
        return imageUrl;
    }

    public void setUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    public int getId() {
        return id;
    }
    public String getInstruction() {
        return instruction;
    }


}
