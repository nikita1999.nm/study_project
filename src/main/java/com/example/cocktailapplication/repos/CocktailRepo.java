package com.example.cocktailapplication.repos;

import com.example.cocktailapplication.domain.Cocktail;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
public interface CocktailRepo extends CrudRepository<Cocktail, Integer> {

}
