package com.example.cocktailapplication.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

    private String name;
    private String url;
    public JSONParser(){

    }
    public JSONParser(String name) {
        this.url= "https://www.thecocktaildb.com/api/json/v1/1/search.php?s="+name;
    }

    public void setUrl(String name){

        this.url="https://www.thecocktaildb.com/api/json/v1/1/search.php?s="+name;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public JSONObject getRandomCocktail() throws IOException {
        setUrl("https://www.thecocktaildb.com/api/json/v1/1/random.php");
        return readJsonFromUrl();
    }

    public  JSONObject readJsonFromUrl() throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        }
        finally {
            is.close();
        }
    }

}
