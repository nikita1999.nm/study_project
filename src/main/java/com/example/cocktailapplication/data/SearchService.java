package com.example.cocktailapplication.data;

import com.example.cocktailapplication.domain.Cocktail;

import java.io.IOException;
import java.util.List;

public class SearchService {

    private String cocktailName;
    private JSONParser jsonParser ;
    private DataCreator dataCreator;
    private Cocktail randomCocktailList[];
    public SearchService(){

    }
    public SearchService(String cocktailName){

        this.cocktailName=cocktailName;
    }
    public String getCocktailName() {

        return cocktailName;
    }

    public void setCocktailName(String cocktailName) {

        this.cocktailName = cocktailName;
    }
    public List<Cocktail> getData() throws IOException {
        dataCreator = new DataCreator(cocktailName);
        return dataCreator.createData();
    }
    public List<Cocktail> getRandomCocktails() throws IOException {
        dataCreator= new DataCreator();
        return dataCreator.getRandomCocktail();
    }


}
