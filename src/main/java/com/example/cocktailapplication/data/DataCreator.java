package com.example.cocktailapplication.data;

import com.example.cocktailapplication.domain.Cocktail;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataCreator {
    private List<Cocktail> data;
    private String cocktailName;
    private JSONArray jsonArray;
    private String imageUrl;
    private String name;
    private String instruction;
    private Cocktail cocktail;
    private List<Cocktail> randomCocktailList;
    private JSONParser jsonParser;
    public DataCreator(){

    }
    public DataCreator(String cocktailName){
        this.cocktailName=cocktailName;
    }


    public List<Cocktail> createData() throws IOException {
        jsonParser= new JSONParser(cocktailName);
        data= new ArrayList<>();
        JSONObject jsonObject= jsonParser.readJsonFromUrl();
        jsonArray = (JSONArray) jsonObject.get("drinks");
        data.clear();
       for (int i = 0; i< jsonArray.length(); i++){
            JSONObject countJsonObject= (JSONObject) jsonArray.get(i);
            imageUrl= countJsonObject.get("strDrinkThumb").toString();
            name = countJsonObject.get("strDrink").toString();
            instruction=countJsonObject.get("strInstructions").toString();
            cocktail= new Cocktail(name, imageUrl,i, instruction);
            data.add(cocktail);
        }
        return data;
    }
    public List<Cocktail> getRandomCocktail() throws IOException {
        char [] array= new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r'};
        char searchSymbol;
        JSONObject jsonObject;
        randomCocktailList= new ArrayList<>();
        jsonParser= new JSONParser();
        for (int i = 0; i< 6; i++){
            searchSymbol= array[(int) ( Math.random() * 17 )];
            jsonParser.setUrl(String.valueOf(searchSymbol));
            jsonObject= jsonParser.readJsonFromUrl();
            jsonArray = (JSONArray) jsonObject.get("drinks");
            jsonObject= (JSONObject) jsonArray.get((int) ( Math.random() * 4 ));
            imageUrl= jsonObject.get("strDrinkThumb").toString();
            name = jsonObject.get("strDrink").toString();
            instruction=jsonObject.get("strInstructions").toString();
            cocktail= new Cocktail(name, imageUrl,i, instruction);
            randomCocktailList.add(cocktail);
        }
        return randomCocktailList;
    }
    /*public char [] getRandomSymbols(){

        int first= (int) ( Math.random() * 7 );
        int second= (int) ( Math.random() * 7 );
        char [] searchArray= new char[]{array[first], array[second]};
        return searchArray;
    }*/
    }

